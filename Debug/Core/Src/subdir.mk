################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/acc_app_integration_stm32.c \
../Core/Src/acc_hal_integration_single_thread_stm32cube_sparkfun_a111.c \
../Core/Src/detector_presence.c \
../Core/Src/example_assembly_test.c \
../Core/Src/example_detector_distance.c \
../Core/Src/main.c \
../Core/Src/ref_app_smart_presence.c \
../Core/Src/stm32g0xx_hal_msp.c \
../Core/Src/stm32g0xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32g0xx.c 

OBJS += \
./Core/Src/acc_app_integration_stm32.o \
./Core/Src/acc_hal_integration_single_thread_stm32cube_sparkfun_a111.o \
./Core/Src/detector_presence.o \
./Core/Src/example_assembly_test.o \
./Core/Src/example_detector_distance.o \
./Core/Src/main.o \
./Core/Src/ref_app_smart_presence.o \
./Core/Src/stm32g0xx_hal_msp.o \
./Core/Src/stm32g0xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32g0xx.o 

C_DEPS += \
./Core/Src/acc_app_integration_stm32.d \
./Core/Src/acc_hal_integration_single_thread_stm32cube_sparkfun_a111.d \
./Core/Src/detector_presence.d \
./Core/Src/example_assembly_test.d \
./Core/Src/example_detector_distance.d \
./Core/Src/main.d \
./Core/Src/ref_app_smart_presence.d \
./Core/Src/stm32g0xx_hal_msp.d \
./Core/Src/stm32g0xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32g0xx.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/acc_app_integration_stm32.o: ../Core/Src/acc_app_integration_stm32.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/acc_app_integration_stm32.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/acc_hal_integration_single_thread_stm32cube_sparkfun_a111.o: ../Core/Src/acc_hal_integration_single_thread_stm32cube_sparkfun_a111.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/acc_hal_integration_single_thread_stm32cube_sparkfun_a111.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/detector_presence.o: ../Core/Src/detector_presence.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/detector_presence.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/example_assembly_test.o: ../Core/Src/example_assembly_test.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/example_assembly_test.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/example_detector_distance.o: ../Core/Src/example_detector_distance.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/example_detector_distance.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/main.o: ../Core/Src/main.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/main.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/ref_app_smart_presence.o: ../Core/Src/ref_app_smart_presence.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/ref_app_smart_presence.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/stm32g0xx_hal_msp.o: ../Core/Src/stm32g0xx_hal_msp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32g0xx_hal_msp.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/stm32g0xx_it.o: ../Core/Src/stm32g0xx_it.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32g0xx_it.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/syscalls.o: ../Core/Src/syscalls.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/syscalls.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/sysmem.o: ../Core/Src/sysmem.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/sysmem.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Core/Src/system_stm32g0xx.o: ../Core/Src/system_stm32g0xx.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu99 -g3 -DSTM32G071xx -DUSE_HAL_DRIVER -DDEBUG -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/rss/include" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/integration" -I"C:/Users/mjaworsk/STM32CubeIDE/workspace_1.5.0/AcconeerA111_G071RB/cortex_m0/examples" -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/system_stm32g0xx.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

