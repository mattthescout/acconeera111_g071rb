var NAVTREE =
[
  [ "Acconeer SDK", "index.html", [
    [ "API", "modules.html", "modules" ],
    [ "Examples", "examples.html", "examples" ],
    [ "Reference Applications", "md__home_ai_jenkins_workspace_sw-main_doc_user_guides_ref_app_ref_app_main.html", "md__home_ai_jenkins_workspace_sw-main_doc_user_guides_ref_app_ref_app_main" ],
    [ "User guides", "md__home_ai_jenkins_workspace_sw-main_doc_user_guides_rss_user_guide_main.html", "md__home_ai_jenkins_workspace_sw-main_doc_user_guides_rss_user_guide_main" ]
  ] ]
];

var NAVTREEINDEX =
[
"assembly.html",
"group__Presence.html#gae929cbe2f26446e548868b9486fd52fc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';