// Copyright (c) Acconeer AB, 2020
// All rights reserved
// This file is subject to the terms and conditions defined in the file
// 'LICENSES/license_acconeer.txt', (BSD 3-Clause License) which is part
// of this source code package.


#ifndef EXAMPLE_MULTIPLE_SERVICE_USAGE_H_
#define EXAMPLE_MULTIPLE_SERVICE_USAGE_H_

#include <stdbool.h>

/**
 * @brief Example for how to use multiple services using the override sensor ID check configuration
 *
 * @return Returns true if successful, otherwise false
 */
bool acc_example_multiple_service_usage(void);


#endif
