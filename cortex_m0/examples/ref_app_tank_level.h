// Copyright (c) Acconeer AB, 2020
// All rights reserved
// This file is subject to the terms and conditions defined in the file
// 'LICENSES/license_acconeer.txt', (BSD 3-Clause License) which is part
// of this source code package.


#ifndef REF_APP_TANK_LEVEL_H_
#define REF_APP_TANK_LEVEL_H_

#include <stdbool.h>

/**
 * @brief Tank level reference application
 *
 * @return Returns true if successful, otherwise false
 */
bool acc_ref_app_tank_level(void);


#endif
