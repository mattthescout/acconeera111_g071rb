// Copyright (c) Acconeer AB, 2019-2020
// All rights reserved
// This file is subject to the terms and conditions defined in the file
// 'LICENSES/license_acconeer.txt', (BSD 3-Clause License) which is part
// of this source code package.


#ifndef EXAMPLE_DETECTOR_PRESENCE_H_
#define EXAMPLE_DETECTOR_PRESENCE_H_

#include <stdbool.h>

/**
 * @brief Presence detector example
 *
 * @return Returns true if successful, otherwise false
 */
bool acc_example_detector_presence(void);


#endif
