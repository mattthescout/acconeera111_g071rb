// Copyright (c) Acconeer AB, 2019-2020
// All rights reserved
// This file is subject to the terms and conditions defined in the file
// 'LICENSES/license_acconeer.txt', (BSD 3-Clause License) which is part
// of this source code package.

#include "main.h"

#include "acc_app_integration.h"


void acc_app_integration_sleep_ms(uint32_t time_msec)
{
	HAL_Delay(time_msec);
}

void acc_app_integration_sleep_us(uint32_t time_usec)
{
	uint32_t time_msec = (time_usec / 1000) + 1;
	HAL_Delay(time_msec);
}
