/*
 * detector_presence.c
 *
 *  Created on: Jan 12, 2021
 *      Author: MJaworski
 *
 *      Copyright (c) Acconeer AB, 2019-2020
 *      All rights reserved
 *      This file is subject to the terms and conditions defined in the file
 *      'LICENSES/license_acconeer.txt', (BSD 3-Clause License) which is part
 *      of this source code package.
 *
 */

#include "stdio.h"
#include "stdbool.h"

#include "detector_presence.h"

#include "acc_detector_presence.h"
#include "acc_definitions.h"
#include "acc_rss.h"
#include "acc_base_configuration.h"
#include "acc_app_integration.h"
#include "acc_hal_integration.h"

/* Range interval */
#define START_SWEEP 		(0.0f)
#define LENGTH_SWEEP 		(1.0f)

/* The update_rate should be high enough to keep up with faster motions.
 * However, doing so will increase the duty cycle and therefore the power consumption of the system. */
#define UPDATE_RATE 		(8)

/* The sweeps_per_frame should also in most cases be set as high as possible, but after the frame rate is set.
 * This will also increase the duty cycle and power consumption of the system. */
#define SWEEPS_PER_FRAME	(16)

/* If you want to distinguish depths with presence from those with no presence,
 * set the sampling_mode to A, otherwise keep it at B (the default) for maximum SNR. */

/* If the gain is too low, objects may not be visible, or it may result in poor signal quality due to quantization errors.
 * If the gain is too high, strong reflections may result in saturated data.
 * We recommend not setting the gain higher than necessary due to signal quality reasons. */
#define GAIN				(0.2f)


/*
 * @file:	detector_presence.c
 * @brief:	This function is used to activate, configure and get data from Acconeer radar
 *
 *  */
bool acc_detector_presence(void) {

	/* RSS activation */
	const acc_hal_t *hal = acc_hal_integration_get_implementation();

	if(!acc_rss_activate(hal)) {
		printf("RSS activate error!\n\r");
		return false;
	}

	/* Presence configuration creation */
	acc_detector_presence_configuration_t presence_config = acc_detector_presence_configuration_create();

	if(!presence_config) {
		printf("Configuration failed!\n\r");
		acc_rss_deactivate();
		return false;
	}

	/* Presence configuration update */
	update_config(presence_config);

	acc_detector_presence_handle_t acc_handle = acc_detector_presence_create(presence_config);

	if(acc_handle == NULL) {
		printf("Failed to create detector\n\r");
		acc_detector_presence_configuration_destroy(&presence_config);
		acc_rss_deactivate();
		return false;
	}

	acc_detector_presence_configuration_destroy(&presence_config);

	if(!acc_detector_presence_activate(acc_handle)) {
		printf("Failed to activate detector\n\r");
		acc_detector_presence_destroy(&acc_handle);
		acc_rss_deactivate();
		return false;
	}

	/* Collecting the results */
	extern acc_detector_presence_result_t result;
//	const int iterations = 200;
	bool correct_result = true;
	bool deactivation = true;

//	for (int i = 0; i < iterations; i++) {
	while(1) {
		correct_result = acc_detector_presence_get_next(acc_handle, &result);

		if(!correct_result) {
			printf("Detector presence get_next() failed\n\r");
			break;
		}

		/* Printing the results */
		print_results(result);
		/* */
		acc_app_integration_sleep_ms(80 / UPDATE_RATE);
	}

	deactivation = acc_detector_presence_deactivate(acc_handle);
	acc_detector_presence_destroy(&acc_handle);
	acc_rss_deactivate();


	return correct_result && deactivation;

}


/*
 * @file:	detector_presence.c
 * @brief:	This function is used to set configuration in Acconeer radar
 *
 *
 *  */
void update_config(acc_detector_presence_configuration_t acc_config) {

	acc_detector_presence_configuration_filter_parameters_t filter_parameters;

	filter_parameters = acc_detector_presence_configuration_filter_parameters_get(acc_config);

	/* If the detection toggles too often, try increasing the output_time_const parameter.
	 * If it is too sluggish, try decreasing it instead.
	 *
	 * param range 0.0f - 1.0f
	 * */
	filter_parameters.output_time_const = 0.8f;

	/* If intra_frame_weight = 1 - detecting fast motion
	 * If intra_frame_weight = 0 - detecting slow motion
	 * If it can’t keep up with the movements, try decreasing the intra_frame_time_const
	 *
	 * param range 0.0f - 1.0f
	 * */
	filter_parameters.intra_frame_weight = 0.5f;

	/* The main configuration of all the services are the profiles, numbered 1 to 5.
	 * The difference between the profiles is the length of the radar pulse and the way the incoming pulse is sampled.
	 * Profiles with low numbers use short pulses while the higher profiles use longer pulses.
		* Profile 1 is recommended for:
    	measuring strong reflectors, to avoid saturation of the received signal
    	close range operation (<20 cm), due to the reduced direct leakage

		* Profile 2 and 3 are recommended for:
    	operation at intermediate distances, (20 cm to 1 m)
    	where a balance between SNR and depth resolution is acceptable

		* Profile 4 and 5 are recommended for:
    	for Sparse service only
    	operation at large distances (>1 m)
    	motion or presence detection, where an optimal SNR ratio is preferred over a high resolution distance measurement
	 */
	acc_detector_presence_configuration_service_profile_set(acc_config, ACC_SERVICE_PROFILE_3);

	/*  */
	acc_detector_presence_configuration_power_save_mode_set(acc_config, ACC_POWER_SAVE_MODE_SLEEP);
	/*  */
	acc_detector_presence_configuration_start_set(acc_config, START_SWEEP);
	/*  */
	acc_detector_presence_configuration_length_set(acc_config, LENGTH_SWEEP);
	/*  */
	acc_detector_presence_configuration_detection_threshold_set(acc_config, DETECTION_THRESHOLD);
	/*  */
	acc_detector_presence_configuration_update_rate_set(acc_config, UPDATE_RATE);
	/*  */
	acc_detector_presence_configuration_filter_parameters_set(acc_config, &filter_parameters);
	/*  */
	acc_detector_presence_configuration_sweeps_per_frame_set(acc_config, SWEEPS_PER_FRAME);
	/*  */
	acc_detector_presence_configuration_receiver_gain_set(acc_config, GAIN);

}

void print_results(acc_detector_presence_result_t result) {
	if(result.presence_detected) printf("Motion detected\n\r");
	else printf("No motion\n\r");

	printf("Presence score: %d, Distance: %d\n\r", (int)(result.presence_score * 1000.0f), (int)(result.presence_distance * 1000.0f));
}
