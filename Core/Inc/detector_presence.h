/*
 * detector_presence.h
 *
 *  Created on: Jan 12, 2021
 *      Author: MJaworsk
 */

#ifndef INC_DETECTOR_PRESENCE_H_
#define INC_DETECTOR_PRESENCE_H_

#include "acc_detector_presence.h"
#include "stdbool.h"

/* The overall sensitivity can be adjusted with the detection_threshold parameter */
#define DETECTION_THRESHOLD	(2.75f)

/* */
bool acc_detector_presence(void);

/* */
void update_config(acc_detector_presence_configuration_t acc_config);

/* */
void print_results(acc_detector_presence_result_t result);

#endif /* INC_DETECTOR_PRESENCE_H_ */
